
const initialState = {
  usersArray: [{
    id: 1,
    firstName: "Nagaraju",
    lastName: "Tirumani",
    title: "Mr",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQR6mNeolQnmtuuUJ3SwppHCm0CfRXTBECfOw&usqp=CAU"
  }, {
    id: 2,
    firstName: "Abinav",
    lastName: "Shukla",
    title: "Mr",
    image: "https://us.123rf.com/450wm/rido/rido1907/rido190700122/127284879-portrait-of-handsome-young-man-in-casual-denim-shirt-keeping-arms-crossed-and-smiling-while-standing.jpg?ver=6"
  }, {
    id: 3,
    firstName: "Prasan",
    lastName: "Dsouza",
    title: "Mr",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTylglJ-TFwuag59gx2q5j62N7Az0V1TduIRQ&usqp=CAU"
  }]
}

export default initialState
