import initialState from "../initState/initState";
import { v4 as uuidv4 } from 'uuid'

function reducer(state = initialState, action) {
    switch (action.type) {
        case "ADD_USER":
            return { ...state, usersArray: [...state.usersArray, { ...action.payload, id: uuidv4() }] }
        case "DELETE_USER":
            return {
                ...state, usersArray: state.usersArray.filter((user) => {
                    return user.id != action.payload.id
                })
            }
        case "UPDATE_USER":
            return {
                ...state, usersArray: state.usersArray.map((user) => {
                    if (user.id == action.payload.id) {
                        return {
                            ...user, firstName: action.payload.firstName,
                            lastName: action.payload.lastName,
                            title: action.payload.title,
                            image: action.payload.image,
                            id: action.payload.id
                        }
                    }
                    return user
                })
            }
        default:
            return state
    }
}

export default reducer
