import FakeData from './FakeData/fakeData'
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import Form from './Form';
import UpdateForm from './UpdateForm'
import './App.css';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route exact path='/' element={<FakeData />} />
        <Route exact path='/form' element={<Form />} />
        <Route exact path='/updateform/:id' element={<UpdateForm />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
