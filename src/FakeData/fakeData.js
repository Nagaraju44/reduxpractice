import React from 'react'
import { connect } from 'react-redux'
import { DeletingUser} from '../Actions/action'
import { Link } from 'react-router-dom'

const FakeData = (props) => {
  const RemoveUser = (event)=>{
    props.DeletingUser(event.target.id)
  }

  return (
    <div className='text-center pb-5 mb-5 mt-5'>
      {props.usersArray.map((user) => {
        return (<div
          key={user.id}
          className="w-100 h-100 mb-4">
          <img src={user.image} className="w-25" alt='user' />
          <h5>{`${user.title}.${user.firstName} ${user.lastName}`}</h5>
          <button className='btn btn-success btn-sm me-3' id={user.id} onClick={RemoveUser}>Remove</button>
          <Link to={`/updateform/${user.id}`}><button className='btn btn-info btn-sm' id={`${user.id}x`} >Edit</button></Link>
        </div>)
      })}
      <Link to="/form"><button className='btn btn-primary'>AddUser</button></Link> 
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    usersArray: state.usersArray
  }
}


const mapDispatchToProps =  {
  DeletingUser
}


export default connect(mapStateToProps, mapDispatchToProps)(FakeData);
