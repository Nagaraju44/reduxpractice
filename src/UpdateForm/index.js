import React, { Component } from 'react'
import { UpdateUser} from '../Actions/action'
import { connect } from 'react-redux'
import validator from 'validator'

class UpdateForm extends Component {
  state = {
    FirstName: "",
    LastName: "",
    title: "select title",
    Url: "",
    Id: window.location.pathname.split('/')[2],
    FirstNameErr: "",
    LastNameErr: "",
    titleErr: "",
    urlErr: "",
    isFirstNameValid: false,
    isLastNameValid: false,
    isTitleValid: false,
    isUrlValid: false,
    isSuccessd: false
  }

  EnteringDetails = (event) => {
    this.setState({ [event.target.id]: event.target.value })
  }

  UpdateUserAfterValidation = () => {
    const { FirstName, LastName, title, Url, Id } = this.state
    if ((this.state.isFirstNameValid === true) &&
      (this.state.isLastNameValid === true) &&
      (this.state.isTitleValid === true) &&
      (this.state.isUrlValid === true)) {
      this.props.UpdateUser(FirstName, LastName, title, Url, Id)
    }
  }

  SignUpValidation = (event) => {
    event.preventDefault()
    const { FirstName, LastName, title, Url } = this.state
    if ((FirstName === "") || (validator.isAlpha(FirstName) === false)) {
      var FirstNameError = "*Enter Valid First Name"
      var FnameValid = false
    } else {
      var FirstNameError = ""
      var FnameValid = true
    }

    if ((LastName === "") || (validator.isAlpha(LastName) === false)) {
      var LastNameError = "*Enter Valid Last Name"
      var LnameValid = false
    } else {
      var LastNameError = ""
      var LnameValid = true
    }

    if ((title === "select title")) {
      var titleError = "*Please Select Valid title"
      var titleValid = false
    } else {
      var titleError = ""
      var titleValid = true
    }

    if ((Url === "") || (validator.isURL(Url) === false)) {
      var urlErr = "*Enter Valid Url"
      var urlValid = false
    } else {
      var urlErr = ""
      var urlValid = true
    }

    if (FnameValid && LnameValid && titleValid && urlValid) {
      var isSuccess = true
    } else {
      var isSuccess = false
    }

    this.setState({
      FirstNameErr: FirstNameError,
      LastNameErr: LastNameError,
      titleErr: titleError,
      urlErr: urlErr,
      isFirstNameValid: FnameValid,
      isLastNameValid: LnameValid,
      isTitleValid: titleValid,
      isUrlValid: urlValid,
      isSuccessd: isSuccess
    }, () => {
      this.UpdateUserAfterValidation()
    })
  }

  render() {
    return (
      <div className="container mt-5">
        <form>
          <div className="mb-1">
            <label htmlFor="Name" className="form-label">FirstName</label>
            <input type="email" className="form-control" id="FirstName" aria-describedby="emailHelp" onChange={this.EnteringDetails} />
            <span className='text-danger'> {this.state.FirstNameErr === "" ? <span>&nbsp;</span> : this.state.FirstNameErr}</span>
          </div>

          <div className="mb-1">
            <label htmlFor="Name" className="form-label">LastName</label>
            <input type="email" className="form-control" id="LastName" aria-describedby="emailHelp" onChange={this.EnteringDetails} />
            <span className='text-danger'> {this.state.LastNameErr === "" ? <span>&nbsp;</span> : this.state.LastNameErr}</span>
          </div>

          <div className="mb-1">
            <label htmlFor="Name" className="form-label">title</label>
            <select className="form-select" aria-label="Default select example" onChange={this.EnteringDetails} id='title'>
              <option value="select title" selected>select title</option>
              <option value="Mr">Mr</option>
              <option value="Miss">Miss</option>
              <option value="Mrs">Mrs</option>
            </select>
            <span className='text-danger'> {this.state.titleErr === "" ? <span>&nbsp;</span> : this.state.titleErr}</span>
          </div>

          <div className="mb-1">
            <label htmlFor="Url" className="form-label">Url</label>
            <input type="url" className="form-control" id="Url" aria-describedby="emailHelp" onChange={this.EnteringDetails} />
            <span className='text-danger'> {this.state.urlErr === "" ? <span>&nbsp;</span> : this.state.urlErr}</span>
          </div>
          {this.state.isSuccessd === true ? <h4 className="text-success">User Updated.Go Back To see the Updated User</h4>
            :
            <button type="submit" className="btn btn-primary" onClick={this.SignUpValidation}>Update</button>} <br />
        </form>

      </div>)

  }
}

const mapDispatchToProps = {
  UpdateUser
}

export default connect(null, mapDispatchToProps)(UpdateForm)
