export function AddingUser(fName, lName, title, url){
    return {
        type: "ADD_USER",
        payload: {
            firstName: fName,
            lastName: lName,
            title: title,
            image: url
        }
    }
}

export function DeletingUser(id){
    return {
        type: "DELETE_USER",
        payload: {
            id: id,
        }
    }
}

export function UpdateUser(fName, lName, title, url, id){
    return {
        type: "UPDATE_USER",
        payload: {
            firstName: fName,
            lastName: lName,
            title: title,
            image: url,
            id: id
        }
    }
}


